#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

DIR=$( cd "$(dirname "$0")" ; pwd - P )

TMPFILE=`mktemp`

trap "rm -f $TMPFILE; exit 1" 0 1 2 3 13 15

crontab -l | sed '/certbot renew/d' | sed '/letsencrypt renew/d' > $TMPFILE  # Capture crontab; delete old entry

cat "$DIR/cron" | sed "s|%this_dir%|$DIR|g" >> $TMPFILE

crontab $TMPFILE

rm -f $TMPFILE

trap 0
