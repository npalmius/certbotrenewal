#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

DIR=$( cd "$(dirname "$0")" ; pwd - P )

LOGFILE="$DIR/renew_cron/certbot_renewal.log"

date >> "$DIR/renew_cron/certbot_renewal.log"

certbot renew | tee -a "$DIR/renew_cron/certbot_renewal.log"

printf "\nOutput saved to $LOGFILE\n"
